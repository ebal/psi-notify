# psi-notify

tl;dr: psi-notify can alert you when resources on your machine are becoming oversaturated, and allow you to take action before your system slows to a crawl.

## Download binary

[ps-notify](https://gitlab.com/ebal/psi-notify/-/jobs/artifacts/master/raw/psi-notify?job=run-build)
